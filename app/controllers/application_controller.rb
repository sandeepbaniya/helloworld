class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

   def hello
    render html: "नमस्ते नेपाल !"
  end
end
